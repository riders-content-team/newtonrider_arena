#ifndef GAZEBO_PLUGINS_HEAT_MAZE_HH_
#define GAZEBO_PLUGINS_HEAT_MAZE_HH_

#include <ignition/math/Pose3.hh>
#include "gazebo/physics/physics.hh"
#include <gazebo/sensors/sensors.hh>
#include "gazebo/common/common.hh"
#include "gazebo/gazebo.hh"

#include <ignition/transport/Node.hh>
#include <gazebo/transport/Node.hh>

#include "ros/ros.h"
#include "newtonrider_arena/SpawnModel.h"
#include "newtonrider_arena/RemoveModel.h"
#include "newtonrider_arena/MoveCart.h"
#include "newtonrider_arena/GetCartStatus.h"
#include "std_srvs/Trigger.h"

#include <vector>

class Cart {
public:
  Cart() : force(0), mass(0), render_x(0), render_z(0), render_angle(0), position_x(0), velocity_x(0), angle(0), mode(1), percentCompleted(0), time(0) {
    
  }
  
  void update(double delta_time);
  
  std::string spawn_name;
  double force;
  double mass;
  double render_x;
  double render_z;
  double render_angle;
  double position_x;
  double velocity_x;
  double angle;
  int mode;
  double percentCompleted;
  double time;
};

namespace gazebo
{
  class GAZEBO_VISIBLE MyPlugin : public WorldPlugin
  {
    // Constructor
    public: MyPlugin();
    
    // Deconstructor
    public: virtual ~MyPlugin();

    // Load Function
    // Runs Once on Initialization
    public: virtual void Load(physics::WorldPtr _world, sdf::ElementPtr _sdf);
  
    // Animations
    private: virtual void Spin();

    // World Reset Callback
    public: virtual void OnReset();

    private: virtual bool SpawnModel(std::string model_name, std::string spawn_name, double pose_x, double pose_y, double pose_z);
    private: virtual bool ServiceSpawnModel(newtonrider_arena::SpawnModel::Request &req, newtonrider_arena::SpawnModel::Response &res);

    private: virtual bool RemoveModel(std::string model_name);
    private: virtual bool ServiceRemoveModel(newtonrider_arena::RemoveModel::Request &req, newtonrider_arena::RemoveModel::Response &res);

    private: virtual bool MoveCart(std::string spawn_name, int mode, double mass, double force);
    private: virtual bool ServiceMoveCart(newtonrider_arena::MoveCart::Request &req, newtonrider_arena::MoveCart::Response &res);

    private: virtual bool ServiceGetCartStatus(newtonrider_arena::GetCartStatus::Request &req, newtonrider_arena::GetCartStatus::Response &res);
    bool SpawnMass(double pose_x, double pose_y, double pose_z, double mass);
    private: void UpdateCarts();

    // Connection to World Update events.
    private: event::ConnectionPtr worldConnection, resetConnection;

    private: std::unique_ptr<ros::NodeHandle> rosNode;

    private: ros::ServiceServer removeModelService;
    private: ros::ServiceServer spawnModelService;
    private: ros::ServiceServer moveCartService;
    private: ros::ServiceServer getCartStatus;
  
    private: physics::WorldPtr world;
    private: physics::ModelPtr mass_model;
    gazebo::transport::NodePtr node_;
    gazebo::transport::PublisherPtr pub_visual_;


    std::vector<Cart> carts;
  };
}
#endif
