#include "MyPlugin.hh"

using namespace gazebo;
GZ_REGISTER_WORLD_PLUGIN(MyPlugin)

#define TRACK_LENGTH 10.0 // length of flat, length of hypotenuse
#define START_POSITION_OFFSET -0.2 // small shift to match how world got set up
#define CART_MASS 1000.0

bool first = true;
MyPlugin::MyPlugin() : WorldPlugin() // Constructor
{
  carts.push_back(Cart()); // add one cart for now - change this then change OnReset
}

MyPlugin::~MyPlugin() // Deconstructor
{
}

// Runs once on initialization
void MyPlugin::Load(physics::WorldPtr _world, sdf::ElementPtr _sdf)
{
    this->world = _world;
    

    if (!ros::isInitialized())
    {
        int argc = 0;
        char **argv = NULL;
        ros::init(argc, argv, "spawn_model_service_node",
                  ros::init_options::NoSigintHandler);
    }

    // Create our ROS node. This acts in a similar manner to the Gazebo node
    this->rosNode.reset(new ros::NodeHandle());

    this->removeModelService = this->rosNode->advertiseService<newtonrider_arena::RemoveModel::Request, newtonrider_arena::RemoveModel::Response>
      ("remove_model", std::bind(&MyPlugin::ServiceRemoveModel, this, std::placeholders::_1, std::placeholders::_2));

    this->spawnModelService = this->rosNode->advertiseService<newtonrider_arena::SpawnModel::Request, newtonrider_arena::SpawnModel::Response>
      ("spawn_model", std::bind(&MyPlugin::ServiceSpawnModel, this, std::placeholders::_1, std::placeholders::_2));

    this->moveCartService = this->rosNode->advertiseService<newtonrider_arena::MoveCart::Request, newtonrider_arena::MoveCart::Response>
      ("move_cart", std::bind(&MyPlugin::ServiceMoveCart, this, std::placeholders::_1, std::placeholders::_2));

    this->getCartStatus = this->rosNode->advertiseService<newtonrider_arena::GetCartStatus::Request, newtonrider_arena::GetCartStatus::Response>
     ("get_cart_status", std::bind(&MyPlugin::ServiceGetCartStatus, this, std::placeholders::_1, std::placeholders::_2));

    // Connect to the world update event.
    this->worldConnection = event::Events::ConnectWorldUpdateBegin(
            std::bind(&MyPlugin::Spin, this));

    // Connect to the world reset event.
    this->resetConnection = event::Events::ConnectWorldReset(
            std::bind(&MyPlugin::OnReset, this));
}

void MyPlugin::UpdateCarts()
{
  double delta_time = 0.001; // TODO how to read this?

  for(size_t n = 0; n < carts.size(); ++n) {
    Cart & cart = carts[n];
    if(cart.mass > 0) {
      cart.update(delta_time);
      gazebo::physics::ModelPtr modelptr = this->world->ModelByName(cart.spawn_name);
      if(modelptr != NULL) {
        auto links = modelptr->GetLinks();
        
        if (first){
          //this->mass_model->SetScale(ignition::math::v4::Vector3d(1,1,sqrt(cart.mass)), true);
          first = false;
        }
        // TODO - add angle
        ignition::math::Pose3d new_pose(0, cart.render_x, cart.render_z, cart.render_angle, 0, 0);
        modelptr->SetLinkWorldPose(new_pose, (*links.begin())); // just one link
        if(cart.render_angle != 0){
          ignition::math::Pose3d new_posee(0, cart.render_x-0.15, cart.render_z+sqrt(cart.mass)*0.1163/20, cart.render_angle, 0, 0);
          this->mass_model->SetWorldPose(new_posee);
        }
        else{
          ignition::math::Pose3d new_posee(0, cart.render_x, cart.render_z+sqrt(cart.mass)*0.1163/20, 0, 0, 0);
          this->mass_model->SetWorldPose(new_posee);
        }
        
        // modelptr->SetWorldPose(new_pose); // also works - any difference in lag? I don't think so - not certain/
      }
    }
  }
}


void MyPlugin::Spin()
{
  if (ros::ok()) {
    UpdateCarts();
    ros::spinOnce();
  }
}

void MyPlugin::OnReset()
{
  RemoveModel("cylinder1");
  carts.clear();
  carts.push_back(Cart()); // add one cart for now
}

bool MyPlugin::MoveCart(std::string spawn_name, int mode, double mass, double force)
{
  // right now just one cart but setup a vector of Carts as may want to extend this later
  Cart & cart = carts[0];
  cart.mass = mass;
  cart.force = force;
  cart.spawn_name = spawn_name;
  cart.mode = mode;
  cart.position_x = 0;
  if(cart.mode == 2) {
    cart.position_x += TRACK_LENGTH;
  }
  
  //this->mass_model->SetScale(ignition::math::v4::Vector3d(1,1,sqrt(cart.mass)/10), true);

  return true;
}

bool MyPlugin::ServiceMoveCart(newtonrider_arena::MoveCart::Request &req, newtonrider_arena::MoveCart::Response &res)
{
    this->mass_model = this->world->ModelByName("cylinder1");
    //this->mass_model->SetScale(ignition::math::v4::Vector3d(1,1,sqrt(req.mass)), true);
    return MoveCart(req.spawn_name, req.mode, req.mass, req.force);
}

bool MyPlugin::ServiceGetCartStatus(newtonrider_arena::GetCartStatus::Request &req, newtonrider_arena::GetCartStatus::Response &res)
{
  Cart & cart = carts[0];
  res.force = cart.force;
  res.mass = cart.mass;
  res.percentCompleted = cart.percentCompleted;
  res.time = cart.time;
  res.mode = cart.mode;

}


bool MyPlugin::SpawnModel(std::string model_name, std::string spawn_name, double pose_x, double pose_y, double pose_z)
{
    std::string pose_x_str = std::to_string(pose_x);
    std::string pose_y_str = std::to_string(pose_y);
    std::string pose_z_str = std::to_string(pose_z);
    std::string mass_str = std::to_string(2);
    std::string size_str = std::to_string(0.4);

    std::string sdf_file_string = "<?xml version='1.0' ?> "
                                  "<sdf version='1.5'>"
                                  "<include>"
                                  "<pose>" + pose_x_str + " " + pose_y_str + " " + pose_z_str + " 0 0 0" + "</pose>"
                                  "<name> " + spawn_name + "</name>"
                                  "<uri>model://" + model_name + "</uri>"
                                  "</include>"
                                  "</sdf>";

    this->world->InsertModelString(sdf_file_string);
    return true;
}

bool MyPlugin::ServiceSpawnModel(newtonrider_arena::SpawnModel::Request &req, newtonrider_arena::SpawnModel::Response &res)
{
    return SpawnModel(req.model_name, req.spawn_name, req.x, req.y, req.z);
}

bool MyPlugin::RemoveModel(std::string model_name)
{
    this->world->RemoveModel(model_name);
    return true;
}

bool MyPlugin::ServiceRemoveModel(newtonrider_arena::RemoveModel::Request &req, newtonrider_arena::RemoveModel::Response &res)
{
    return RemoveModel(req.model_name);
}

void Cart::update(double delta_time) {
  if(mass > 0) {
    double gravity = 1.3734; // m/s^2 for Titan
    double track_length = TRACK_LENGTH;
    double track_angle = M_PI / 6.0; // 30 degrees

    double cosa = cos(track_angle);
    double sina = sin(track_angle);
    double tana = sina / cosa;

    double length_final_flat = 2.0; // hits a barrier and stops
    double horizontal_incline_length = track_length * cosa;

    double M = CART_MASS + mass;

    if(position_x > track_length && position_x < track_length + horizontal_incline_length) {
      // total_force is along the hypotenuse
      double total_force = force; // cart force is now along hypotenuse
      total_force -= M * gravity * sina; // component of gravity
      double accel = total_force / M; // along hypotenuse
      velocity_x += accel * delta_time; // shift x as component of accel
      position_x += velocity_x * cosa * delta_time; // will fix z as locked to track
    }
    else {
      double acceleration_x = force / M;
      if(percentCompleted >= 1.0) {
        velocity_x *= 0.999; // apply friction bring cart to a quick stop
        acceleration_x = 0;
      }

      velocity_x += acceleration_x * delta_time;
      position_x += velocity_x * delta_time;
    }

    double cart_z_offset = 0.42;
    double cart_z_offset_incline = 0.65; // cart center is not center so just hard code it

    angle = 0;
    double position_z = 0;
    if(position_x < track_length) {
      position_z = 0; // on the flat
    }
    else if(position_x > track_length + horizontal_incline_length) {
      position_z = track_length * sina; // on the second flat
      if(position_x > track_length + horizontal_incline_length + length_final_flat) {
        position_x = track_length + horizontal_incline_length + length_final_flat; // clamp it
      }
    }
    else {
      // we are on the incline
      position_z = (position_x - track_length) * tana;
      angle = track_angle;
    }

    // target render angle
    double angle_margin = 0.22; // interpolate over this distance
    double shifter1 = 0.2; // some nonsense to make the cart change angles in a visually appealling way
    double shifter2 = 0.2; // some nonsense to make the cart change angles in a visually appealling way
    double target_render_z_offset = (position_x > track_length && position_x < track_length + horizontal_incline_length) ? cart_z_offset_incline : cart_z_offset;
    double target_render_angle = (position_x > track_length && position_x < track_length + horizontal_incline_length) ? track_angle : 0.0;
    if(position_x > track_length - angle_margin - shifter1 && position_x < track_length + angle_margin - shifter1) {
      double delta = position_x - (track_length - angle_margin - shifter1);
      double percent = delta / (2.0 * angle_margin);
      target_render_angle = track_angle * percent;
      target_render_z_offset = cart_z_offset + (cart_z_offset_incline - cart_z_offset) * percent;
    }
    if(position_x > track_length + horizontal_incline_length - angle_margin - shifter2 && position_x < track_length + horizontal_incline_length + angle_margin - shifter2) {
      double delta = position_x - (track_length + horizontal_incline_length - angle_margin - shifter2);
      double percent = delta / (2.0 * angle_margin);
      target_render_angle = track_angle - track_angle * percent;
      target_render_z_offset = cart_z_offset_incline + (cart_z_offset - cart_z_offset_incline) * percent;
    }
    double target_render_z = position_z + target_render_z_offset;

    // interpolate
    double alpha = 0.1;
    // snap if object restarts
    if(render_z == 0) {
      alpha = 1.0; // intialize
    }

    render_angle += (target_render_angle - render_angle) * alpha;
    render_x += (position_x + START_POSITION_OFFSET - render_x) * alpha;
    render_z += (target_render_z - render_z) * alpha;
    
    double old_percent_completed = percentCompleted;

    switch(mode) {
      case 1: // flat
        percentCompleted = position_x / track_length;
        break;
      case 2: // incline
        percentCompleted = (position_x - track_length) / horizontal_incline_length;
        break;
      case 3: // flat + incline
        percentCompleted = position_x / (track_length + horizontal_incline_length);
        break;
    }

    if(percentCompleted < 0) {
      percentCompleted = 0;
    }

    if(mode == 1 && velocity_x > 0 && position_x > track_length) {
      velocity_x = -velocity_x * 0.85; // bounce off the barrier
      percentCompleted = 1.0; // code for complete done
    }

    if(percentCompleted < old_percent_completed) {
      percentCompleted = old_percent_completed;
    }

    if(percentCompleted < 1.0) {
      time += delta_time;
    }
  }
}
